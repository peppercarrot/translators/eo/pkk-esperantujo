# Pipro kaj Karoĉjo / pkk-esperantujo

Tiu PKK-Esperantujo estas kiel sabloludujo, por kunlabori pri la tradukado aŭ Esperantigado de la bildstrio [_Pepper&Carrot_](https://peppercarrot.com), kiun David Revoy desegnas kaj aperigas sur sian TTT-ejon de 2014.

# Enhavo

* Unue, tiu Git-ujo enhavas la EO-dosierojn.

* Se iu EO-dosierujo ne ĉeestas, tio signifas, ke neniu tradukis tiun rakonteton.

* Vidu ankaŭ la anglajn kaj la francajn dosierojn en [la ĉefa PKK-Gitujo]( https://framagit.org/peppercarrot/webcomics) ĉar tiuj lingvoj estas pretaj antaŭ ol la aliaj.

Se vi ne konas la francan lingvon, tio ne gravas. Uzu la anglan tekston.

La antaŭajn rakontetojn vi povas legi en tiaj diversaj lingvoj, kiajn vi konas aŭ volas lerni en [la ĉefa PKK-Gitujo]( https://framagit.org/peppercarrot/webcomics).

# PKK-ujo ĉe Framagito

Se vi ne konas Giton kaj ne povas ĉiam uzi Framagiton (Git), vi povas partopreni [tiun forumon](https//librefan.eu.org/forum "Ĉe Libre-Fan"), kiu estas tiel libera kiel Framagito sed eble pli facila por vi.

# Permesilo

La enhavo de tiu Git-ujo estas disponebla laŭ la permesilo [Krea Komunaĵo Atribuite 4.0 Tutmonda (CC BY-SA 4.0)](https://creativecommons.org/licenses/by/4.0/deed.eo), escepte la dosieroj en la [`fonts/`-ujo](https://framagit.org/peppercarrot/webcomics/tree/master/fonts) (la tiparujo), havantaj siajn proprajn permisilojn.

Se vi proponas vian verkon en tiu Git-ujo, bonvolu konsenti la [kontrakton por la kunlaboruloj](CONTRIBUTING.md).


# Rimarkoj

Kvankam vi devas uzi la permesilon [Krean Komunaĵon Atribuite 4.0 Tutmondan (CC BY) 4.0)](https://creativecommons.org/licenses/by/4.0/) ĉi tie, aliajn permesilojn vi povas uzi, ekzemple, ĉe via TTT-ejo.

Tiele, iu uzas la permesilon [Krean Komunaĵon Atribuite-Samkondiĉe 4.0 Tutmondan (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.eo) ĉe [sia TTT-ejo](librefan.eu.org).

Se vi volas eldoni _PkK_ -on iel ajn, aŭ fari ŝanĝojn aŭ verki aliigadojn, vi nur devas mencii la aŭtoron, David Revoy, kaj sian permesilon.

# EO-kunlaboruloj 

Bonvolu vidu la dosieron, [EO-Aŭtoroj.md](EO-AUXTOROJ.md).

La plenan aŭtor-dosieron enhavas [la ĉefa PKK-Gitujo]( https://framagit.org/peppercarrot/webcomics). Vi povas aldoni vian kaŝnomon tie se ĝi ne estas tie.

# Pri la tradukado

* Se vi komprenas la anglan, vidu [CONTRIBUTING.md](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md)

* Se vi ne komprenas la anglan, aldonu [novan “Issue”-on ĉi-tie](https://framagit.org/peppercarrot/translators/eo/pkk-esperantujo/issues).

*Issue* estas solvota demando aŭ malfacilaĵo. La Issue-ejo estas preskaŭ sama kiel forumo.
 
====

# Angle
Content in this repository is licensed under the [Creative Commons Attribution 4.0 International - CC-By](https://creativecommons.org/licenses/by/4.0/) License, except for the files in the [`fonts/` directory](https://framagit.org/peppercarrot/webcomics/tree/master/fonts), which are released under their own separate license agreements.

By submitting content to this repository, you agree to the [contributor's terms](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md).

